import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueHtmlToPaper from 'vue-html-to-paper'
import axios from 'axios'
import VueAxios from 'vue-axios'
import '@/assets/scss/style.scss'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios)
Vue.use(VueHtmlToPaper, options)
// or using the defaults with no stylesheet
Vue.use(VueHtmlToPaper)
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

import Vue from 'vue'
import VueRouter from 'vue-router'
import Q1 from '../views/Q1.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Q1',
    component: Q1
  },
  {
    path: '/Q2',
    name: 'Q2',
    component: () => import('../views/Q2.vue')
  },
  {
    path: '/Q3',
    name: 'Q3',
    component: () => import('../views/Q3.vue')
  },
  {
    path: '/Q4',
    name: 'Q4',
    component: () => import('../views/Q4.vue')
  },
  {
    path: '/Q5',
    name: 'Q5',
    component: () => import('../views/Q5.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
